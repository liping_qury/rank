# coding=utf-8
"""
调用排序模型，对items排序打分，计算applist
"""
import re
import sys
import math
import numpy as np
from elasticsearch import Elasticsearch
from ext_feature import Feature
from lambdamart import LambdaMART
from mysqlSearch import mymysql

import json
from tornado.options import define, options
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

define("port", default=8080, help="run on the given port", type=int)
es = Elasticsearch([{'host': '34.87.48.140', 'port': 6666}], http_auth=('elastic', 'quryes6666'), timeout=3600)


def appSearch():
    """
    app class 提取
    :return:
    """
    topic = {"news":["sports_news", "art_and_entertainment_news", "social_news", "technology_news", "business_news", "political_news", "else_news"],
             "shopping":["daily_necessities", "fashion", "electronic_product", "leisure_products", "car", "else_shopping"],
             "life": ["healthcare_services", "jobs", "education", "activity", "menu", "trip", "else_life"],
             "entertainment": ["game", "video", "audio", "comic_books", "else_entertainment"]}
    topic_index = {}
    for k, v in topic.items():
        for s in v:
            topic_index[s] = k
    search_command = 'select tag, app_package_id from tag_applist'
    m = mymysql()
    tag_app = m.search(search_command)
    app_class = {}
    for item in tag_app:
        cla = topic_index.get(item[0], "else")
        app_class[item[1]] = cla
    return app_class


def load_tag():
    """
    加载 train query 分类数据，当前仅用于训练测试，之后需要补充该文件
    :return:
    """
    tag_map = {}
    with open(tag_file, 'r') as f:
        line = f.readline()
        while(line):
            line_ = line.strip().split('\t')
            line = f.readline()
            if line_[0] in tag_map: continue
            if len(line_)==2 and (line_[1] in ['else', 'knowledge']):
                line_.append('nn')
            if len(line_) != 3:
                print(line_)
                continue
            tag_map[line_[0].lower()] = line_[1:3]

    print('train query数据个数 %d' % len(tag_map))
    return tag_map


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        json_byte = self.request.body
        input = json.loads(json_byte)
        query = input.get('query', '')
        applist = rankapp(query)
        self.write(applist)
    def post(self):
        #query = self.get_argument('stemQuery', '')
        #train_query = self.get_argument('displayQueryList', '')
        json_byte = self.request.body   # json读取
        input = json.loads(json_byte)
        query = input.get('query', '')
        applist = rankapp(query)
        # self.write(dumps(displayQuery2ScoreMap))
        self.write(applist)

    def rankapp(self, query):
        topn = 30
        app_score = self.rank(query)
        if len(app_score)>30:
            app_score = app_score[:30]
        return app_score


    def request_item(self, query, tags):
        """
        提取 query 的 items 信息
        :param query:
        :param tags:
        :return:
        """
        query_items = []
        # 查看query对应的items数量，进行分页提取（速度快）
        query_json = {"query": {"match": {"query": query}}, "size": 0}
        es_data = es.search(index='inverted_items_prod', body=query_json)  # 提取 item 数
        try:
            num = es_data.get('hits').get('total').get('value')
        except:
            num = 0
        # 分页查询，每次300，最多取1000
        num = min(num, 2000)
        start = 0
        interval = 500
        while(start < num):
            query_json = {"query": {"match": {"query": query}}, "from": start, "size": interval}
            es_data = es.search(index='inverted_items_prod', body=query_json)   # 提取 1000 item
            Hits = es_data.get('hits').get('hits')

            # 两个默认值
            qid = 10001
            score = 0
            for items in Hits:
                itemIconPath = items["_source"]["itemIconPath"]
                picture = 0
                if itemIconPath: picture = 1
                app = items["_source"]["appPackageId"]
                itemAppRank = str(items["_source"]["itemAppRank"])
                itemTitle = items["_source"]["itemTitle"]
                itemLink = items["_source"]["itemLink"]
                line = [qid, query, tags[0], tags[1], app, itemAppRank, picture, itemTitle, itemLink, score]
                query_items.append(line)
            start += interval
        return query_items

    def rank(self, query):
        app_score = {}

        # predict score
        tags = tag_map.get(query, ['else', ''])
        query_items = self.request_item(query, tags)
        #print('es import %d items' % len(query_items))
        if len(query_items) == 0:
            return app_score
        featureList = FEATURE.get_feature(query_items)
        predicted_scores = model.predict(np.array(featureList)[:, 1:])

        # app rank: app下的item平均分排序
        # query下 <app,score>计算
        for i in range(len(predicted_scores)):
            appid = query_items[i][4]
            item_score = app_score.get(appid, [])
            item_score.append([query_items[i][1], predicted_scores[i]])
            app_score[appid] = item_score
        # app 打分--分类得分 + (item的平均分* item数量权重) 【惩罚：item少于3】
        for appid in app_score.keys():
            title_score = app_score[appid]
            item_score = [e[1] for e in title_score]
            titles = [e[0].lower() for e in title_score]
            t_score = 0.0
            N = min(4, len(titles))
            query = query.lower()
            for i in range(N):
                if query in titles[i]:
                    t_score += 1
            t_score = t_score / N / 2

            #item_score = app_score[appid]

            ind = min(len(item_score), 5)
            item_score_ = item_score[:ind]

            score = t_score + np.average(item_score_) * math.pow(len(item_score), 0.3) / 2
            cla = FEATURE.app_class.get(appid, [{'else'}, {'nn'}])
            if tags[0] in cla[0]: score += 0.1
            if tags[1] in cla[1]: score += 0.2
            if len(item_score) < 3: score -= 0.2
            if score < -1: score = -1.0
            elif score > 2: score = 2.0

            #app_score[appid] = [score, np.average(item_score_), math.pow(len(item_score), 0.3), cla]
            app_score[appid] = score
        # app 排序
        app_score = sorted(app_score.items(), key=lambda d: d[1], reverse=True)
        #app_score = sorted(app_score.items(), key=lambda d: d[1][0], reverse=True)

        #print("query category: %s" % ('  '.join(tags)))  ##

        return app_score


if __name__ == "__main__":
    tag_file = './tq_category.dat'   # 分类文件，需要补充替换   tq_category.dat
    model_file = 'example_model.lmart'

    tag_map = load_tag()
    FEATURE = Feature()
    model = LambdaMART()
    model.load(model_file)

    AR = appListRank()
    topn = 30
    k = 0
    

