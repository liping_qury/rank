# coding=utf-8
"""
根据 query-items 数据计算item的特征向量
"""
import math
from SemanticCorrelation import SemanticCorrelation
from mysqlSearch import mymysql


class Feature:
    def __init__(self):
        self.first_domain_index = {'else': 0, 'news': 1, 'shopping': 2, 'life': 3, 'entertainment': 4, 'knowledge': 5}
        self.domain_len = len(self.first_domain_index)
        self.data = []
        self.SC = SemanticCorrelation()
        self.app_class = self.appSearch()

    def load_data(self, file):
        """
        加载es数据：query对应的每个item的原始特征数据（appid，有无图片，link等）
        :return:
        """
        qid = 10000
        ori = ""
        with open(file, 'r') as f:
            line = f.readline()
            while line:
                line = line.strip().split('\t')
                if line[0] != ori:
                    qid += 1
                    ori = line[0]
                self.data.append([qid] + line)
                line = f.readline()

    def appSearch(self):
        """
        app class 提取
        :return:
        """
        topic = {
            "news": ["sports_news", "art_and_entertainment_news", "social_news", "technology_news", "business_news",
                     "political_news", "else_news"],
            "shopping": ["daily_necessities", "fashion", "electronic_product", "leisure_products", "car",
                         "else_shopping"],
            "life": ["healthcare_services", "jobs", "education", "activity", "menu", "trip", "else_life"],
            "entertainment": ["game", "video", "audio", "comic_books", "else_entertainment"]}
        topic_index = {}
        for k, v in topic.items():
            for s in v:
                topic_index[s] = k
        search_command = 'select tag, app_package_id from tag_applist'
        m = mymysql()
        tag_app = m.search(search_command)
        app_class = {}
        for item in tag_app:
            cla1 = topic_index.get(item[0], "else")
            cla2 = item[0] if cla1 != "else" else 'nn'
            app_class.setdefault(item[1], [set(), set()])
            app_class[item[1]][0].add(cla1)
            app_class[item[1]][1].add(cla2)
            #self.app_class[item[1]]= [cla1, cla2]
        print("app category extract over")
        return app_class

    def get_distance(self, query, title):
        q_items = query.lower().split(' ')
        t_items = set(title.lower().split(' '))
        acount = 0.0
        hit = 0.0
        for item in q_items:
            idf = self.SC.idf_dic.get(item, 0.8)
            acount += idf
            if item in t_items:
                hit += idf
        if acount > 0.0:
            return hit/acount
        else:
            return 0.0

    def get_feature(self, data):
        """
        计算 单个item 的特征向量
        :return:
        """
        feature_list = []
        i = 1
        #print('all need extect num = %d' % len(data))
        for item in data:
            if i % 10000 == 0: print('extect %d lines' % i)
            i += 1

            if (len(item) != 10):
                print(item)
                continue
            qid, query, fir_clc, sec_clc, app_id, app_rank, has_picture, title, link, score = item
            first_domain = [0.0] * self.domain_len
            class_1 = self.first_domain_index.get(fir_clc, 0)
            first_domain[class_1] = 1.0
            rank = math.pow(float(app_rank) + 1, 0.4)

            correlation = self.SC.clc_relevancy(query, title)
            quality = 1.0 if len(title.split()) > 4 else 0.0
            cla = self.app_class.get(app_id, [set('else'), set('nn')])
            category_coincide1 = 1.0 if fir_clc in cla[0] else 0.0
            category_coincide2 = 1.0 if sec_clc in cla[1] else 0.0
            distance_idf = self.get_distance(query, title)

            # ------添加特征集------ #
            # label qid，一级分类6，一级分类一致性，二级分类一致性，item在原app中的排序，是否有图片，语义相关性，编辑距离（title包含query的word的比率），item的质量
            feature_single = [score, qid] + first_domain + [category_coincide1, category_coincide2, rank, has_picture,
                                                            correlation, distance_idf, quality]
            feature_list.append(feature_single)
        return feature_list

    def feature_collection(self, file):
        """
        计算并将特征向量保存于文件
        :param file:
        :return:
        """
        f = open(file, 'w')
        feature_list = self.get_feature(self.data)
        for item in feature_list:
            feature_single = [str(i + 1) + ':' + str(item[i + 2]) for i in range(len(item) - 2)]
            line = [item[0], 'qid:' + str(item[1])] + feature_single
            f.write(' '.join(line) + '\n')


if __name__ == "__main__":
    label_file = '/Users/edz/Desktop/query/data/item_labeled.dat'
    feature_file = '/Users/edz/Desktop/query/data/item_feature.dat'

    Fe = Feature()
    Fe.load_data(label_file)
    Fe.feature_collection(feature_file)
