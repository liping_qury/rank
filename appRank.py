# coding=utf-8
"""
调用排序模型，对items排序打分，计算applist
"""
import re
import sys
import math
import json
import requests
import numpy as np
from elasticsearch import Elasticsearch
from ext_feature import Feature
from lambdamart import LambdaMART
from mysqlSearch import mymysql

es = Elasticsearch([{'host': '34.87.48.140', 'port': 6666}], http_auth=('elastic', 'quryes6666'), timeout=3600)


def appSearch():
    """
    app class 提取
    :return:
    """
    topic = {"news":["sports_news", "art_and_entertainment_news", "social_news", "technology_news", "business_news", "political_news", "else_news"],
             "shopping":["daily_necessities", "fashion", "electronic_product", "leisure_products", "car", "else_shopping"],
             "life": ["healthcare_services", "jobs", "education", "activity", "menu", "trip", "else_life"],
             "entertainment": ["game", "video", "audio", "comic_books", "else_entertainment"]}
    topic_index = {}
    for k, v in topic.items():
        for s in v:
            topic_index[s] = k
    search_command = 'select tag, app_package_id from tag_applist'
    m = mymysql()
    tag_app = m.search(search_command)
    app_class = {}
    for item in tag_app:
        cla = topic_index.get(item[0], "else")
        app_class[item[1]] = cla
    return app_class


def load_tag():
    """
    加载 train query 分类数据，当前仅用于训练测试，之后需要补充该文件
    :return:
    """
    tag_map = {}
    with open(tag_file, 'r') as f:
        line = f.readline()
        while(line):
            line_ = line.strip().split('\t')
            line = f.readline()
            if line_[0] in tag_map: continue
            if len(line_)==2 and (line_[1] in ['else', 'knowledge']):
                line_.append('nn')
            if len(line_) != 3:
                print(line_)
                continue
            tag_map[line_[0].lower()] = line_[1:3]

    print('train query数据个数 %d' % len(tag_map))
    return tag_map


# 需要提取subtitle的app
video_dic = {'com.shazam.android', 'tunein.player', 'com.genius.android', 'my.googlemusic.play', 'com.datpiff.mobile',
             'com.musixmatch.android.lyrify', 'com.smule.singandroid', 'com.soundcloud.android', 'com.tubitv',
             'com.showtime.standalone', 'air.com.vudu.air.DownloaderTablet',
             'com.disney.datg.videoplatforms.android.abc', 'com.amazon.avod.thirdpartyclient',
             'com.google.android.youtube', 'com.acowboys.oldmovies', 'com.open.loading'}


class appListRank:

    def request_item(self, query, tags):
        """
        提取 query 的 items 信息
        :param query:
        :param tags:
        :return:
        """
        query_items = []
        # 查看query对应的items数量，进行分页提取（速度快）
        query_json = {"query": {"match": {"query": query}}, "size": 0}
        es_data = es.search(index='inverted_items_prod', body=query_json)  # 提取 item 数
        try:
            num = es_data.get('hits').get('total').get('value')
        except:
            num = 0
        # 分页查询，每次300，最多取1000
        num = min(num, 2000)
        start = 0
        interval = 500
        while(start < num):
            query_json = {"query": {"match": {"query": query}}, "from": start, "size": interval}
            es_data = es.search(index='inverted_items_prod', body=query_json)   # 提取 1000 item
            Hits = es_data.get('hits').get('hits')

            # 两个默认值
            qid = 10001
            score = 0
            for items in Hits:
                itemIconPath = items["_source"]["itemIconPath"]
                picture = 0
                if itemIconPath: picture = 1
                app = items["_source"]["appPackageId"]
                itemAppRank = str(items["_source"]["itemAppRank"])
                itemTitle = items["_source"]["itemTitle"]
                subtitle = items["_source"]["itemSubTitle"]
                if app in video_dic:
                    if itemTitle and subtitle:
                        itemTitle = itemTitle + ' ' + subtitle
                    elif subtitle:
                        itemTitle = subtitle
                itemLink = items["_source"]["itemLink"]
                line = [qid, query, tags[0], tags[1], app, itemAppRank, picture, itemTitle, itemLink, score]
                query_items.append(line)
            start += interval
        return query_items


    def request_item2(self, query, tags):
        """
        sql中提取 query 的 items 信息
        :param query:
        :param tags:
        :return:
        """
        query_items = []
        if query not in app_country:
            print(query)
            print('app_country error …… ')
        ac_list = app_country.get(query, [])
        if len(ac_list) == 0:
            return []
        for ac in ac_list:
            data = {"appList": [{
                "appPackageId": ac[0],
                "displayQuery": query,
                "country": ac[1],
                "startIndex": 0,
                "offset": 50
            }]}
            aheaders = {'Content-Type': 'application/json'}
            url = "http://34.126.123.149:20141/search"
            response = requests.post(url, headers=aheaders, data=json.dumps(data))
            text = json.loads(response.text)
            itemlist = text['data'][0]['appItemList']

            # 两个默认值
            qid = 10001
            score = 0
            for items in itemlist:
                itemIconPath = items["itemIconPath"]
                picture = 0
                if itemIconPath: picture = 1
                app = ac[0]
                itemAppRank = 10
                itemTitle = items["itemTitle"]
                subtitle = items["itemSubTitle"]
                if app in video_dic:
                    if itemTitle and subtitle:
                        itemTitle = itemTitle + ' ' + subtitle
                    elif subtitle:
                        itemTitle = subtitle
                itemLink = items["itemLink"]
                line = [qid, query, tags[0], tags[1], app, itemAppRank, picture, itemTitle, itemLink, score]
                query_items.append(line)
        return query_items


    def rank(self, query):
        app_score = {}

        # predict score
        tags = tag_map.get(query, ['else', ''])
        query_items = self.request_item(query, tags)
        #print('es import %d items' % len(query_items))
        if len(query_items) == 0:
            return app_score
        featureList = FEATURE.get_feature(query_items)
        predicted_scores = model.predict(np.array(featureList)[:, 1:])

        # app rank: app下的item平均分排序
        # query下 <app,score>计算
        for i in range(len(predicted_scores)):
            appid = query_items[i][4]
            item_score = app_score.get(appid, [])
            item_score.append([query_items[i][7], predicted_scores[i]])
            app_score[appid] = item_score
        # app 打分--分类得分 + (item的平均分* item数量权重) 【惩罚：item少于3】
        for appid in app_score.keys():
            title_score = app_score[appid]
            item_score = [e[1] for e in title_score]
            titles = [e[0].lower() for e in title_score]
            t_score = 0.0
            N = min(4, len(titles))
            query = query.lower()
            for i in range(N):
                if query in titles[i]:
                    t_score += 1
            t_score = t_score / N * 0.7 # / *0.5     ###

            #item_score = app_score[appid]

            ind = min(len(item_score), 5)
            item_score_ = item_score[:ind]

            score = t_score + np.average(item_score_) * math.pow(len(item_score), 0.3) *0.3 # *0.5   ###
            cla = FEATURE.app_class.get(appid, [{'else'}, {'nn'}])
            if tags[0] in cla[0]: score += 0.1
            if tags[1] in cla[1]: score += 0.2
            if len(item_score) < 3: score -= 0.2
            if score < -1: score = -1.0
            elif score > 2: score = 2.0
            #app_score[appid] = [score, np.average(item_score_), math.pow(len(item_score), 0.3), cla]
            app_score[appid] = round(score, 2)
            #print(appid + '\t' + str(score)+'\t'+str(t_score)+'\t'+'|||'.join(titles[:N]))
            
        # app 排序
        app_score = sorted(app_score.items(), key=lambda d: d[1], reverse=True)
        #app_score = sorted(app_score.items(), key=lambda d: d[1][0], reverse=True)

        #print("query category: %s" % ('  '.join(tags)))  ##

        return app_score


def load_hw_top(file):
    hw_top = set()
    f = open(file, 'r')
    for line in f.readlines():
        hw_top.add(line.strip().lower())
    return hw_top

def load_app_country():
    app_country = {}
    with open(app_country_file, 'r') as f:
        line = f.readline()
        while line:
            line = line.strip().split('\t')
            if len(line) != 3:
                print(line)
            line[0] = line[0].lower()
            ac = app_country.get(line[0], [])
            ac.append(line[1:])
            app_country[line[0]] = ac
            line = f.readline()
    return app_country


if __name__ == "__main__":
    tag_file = '../../send_data/tq_category.dat'#category_all_hw_top.dat'#tq_category_20w.dat'   # 分类文件，需要补充替换   tq_category.dat
    out_file = './appList.dat'#appList_hw_top.dat'
    model_file = 'example_model.lmart'
    app_country_file = '../../send_data/query_20w_app_country.dat'#hw_top_app_country.dat'#query_20w_app_country.dat'
    #app_country = load_app_country()


    tag_map = load_tag()
    FEATURE = Feature()
    model = LambdaMART()
    model.load(model_file)

    AR = appListRank()
    hw_top = load_hw_top('./hw_top_query.dat')

    f = open(out_file, 'w')
    topn = 50
    k = 0
    # hw_top_query
    
    for query in hw_top:
        if k % 100 == 0: print('rank train query %d' % k)
        k += 1
        query = query.lower()
        app_score = AR.rank(query)
        for i in range(len(app_score)):
            if i > topn: break
            f.write(query + '\t' + app_score[i][0] + '\t' + str(app_score[i][1]) + '\n')
    
    # train query
    for query in tag_map.keys():
        query = query.lower()
        #if query != 'luis alfonso de alba': continue

        if k % 100 == 0:
            print('rank train query %d' % k)
        k += 1

        app_score = AR.rank(query)
        for i in range(len(app_score)):
            if i > topn: break
            #print(query + '\t' + app_score[i][0] + '\t' + str(app_score[i][1]))
            f.write(query + '\t' + app_score[i][0] + '\t' + str(app_score[i][1]) + '\n')



    """
    print('------ All data&model load over, please input train query to test ------')
    topn = 30
    for line in sys.stdin:
        query = line.strip().lower()
        app_score = AR.rank(query)
        print('------recall app num %d, next list top %d------' % (len(app_score), topn))
        for i in range(len(app_score)):
            if i > topn: break
            print(app_score[i][0] + '\t' + str(app_score[i][1]))
    """




