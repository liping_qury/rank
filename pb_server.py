#coding=utf-8
"""<ori_query, train_query> correlation calculation"""
import re
import math
import numpy as np
import json
from tornado.options import define, options
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from queryClass import QueryClass

define("port", default=8080, help="run on the given port", type=int)

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        query = self.get_argument('query', '')
        cls = qc.fun(query)
        self.write(' '.join(cls))
    def post(self):
        query = self.get_argument('query', '')
        cls = qc.fun(query)
        # self.write(dumps(displayQuery2ScoreMap))
        self.write(' '.join(cls))





if __name__ == "__main__":
    qc = QueryClass()
    
    print('server is started ……')
    tornado.options.parse_command_line()
    app = tornado.web.Application(handlers=[(r"/query/category", IndexHandler)])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
