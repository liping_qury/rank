# coding=utf-8
"""
sql查询，插入
"""
import pymysql
import traceback


class mymysql:
    def __init__(self):
        self.conn = pymysql.connect(
            host="34.126.69.190",  #"34.126.92.3",
            port=3306,
            user="java",
            passwd="W@wX0UgaHp4E",
            db="search_item_new"#"search_materiel")
        )

    def insert(self, command, data):
        cur = self.conn.cursor()
        try:
            cur.executemany(command, data)
            self.conn.commit()
        except:
            self.conn.rollback()
            traceback.print_exc()
        finally:
            cur.close()

    def insert_single(self, command):
        cur = self.conn.cursor()
        cur.execute(command)
        self.conn.commit()

    def search(self, command):
        cur = self.conn.cursor()
        try:
            cur.execute(command)
            res = cur.fetchall()
        except:
            res = []
        finally:
            cur.close()
        return res


if __name__ == '__main__':
    search_command = "select tag, app_package_id from tag_applist where id<10"
    m = mymysql()
    data = m.search(search_command)

    print('search over')
