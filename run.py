# coding=utf-8
"""
执行训练，保存模型，模型测试
"""
import re
import math
import numpy as np
from lambdamart import LambdaMART


def get_data(file_loc):
    f = open(file_loc, 'r')
    data = []
    for line in f.readlines():
        new_arr = []
        arr = line.split(' #')[0].split()
        ''' Get the score and query id '''
        score = arr[0]
        q_id = arr[1].split(':')[1]
        new_arr.append(int(score))
        new_arr.append(int(q_id))
        arr = arr[2:]
        ''' Extract each feature from the feature vector '''
        for el in arr:
            new_arr.append(float(el.split(':')[1]))
        data.append(new_arr)
        f.close()
    return np.array(data)


def train():
    training_data = get_data("../data/item_feature.dat")
    model = LambdaMART(
        training_data=training_data,
        number_of_trees=50,
        learning_rate=0.06)
    model.fit()
    model.save('example_model')

def predict():
    model = LambdaMART()
    model.load('example_model.lmart')
    test_data = get_data("../data/item_feature_test.dat")
    predicted_scores = model.predict(test_data[:, 1:])
    print(predicted_scores)


if __name__ == "__main__":

    train()

    predict()

