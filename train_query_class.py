# coding=utf-8
"""
train query 分类（先过词典，再过模型）
"""
import re
import sys
import math
import numpy as np
import csv
from mysqlSearch import mymysql
from pb_bert import pb_predict
from multiprocessing import Pool


def load_tq(csv_file):
    tq = set()
    with open(csv_file, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            if len(row) == 1:
                tq.add(row[0])
    f_out = open('train_query_30w.dat', 'w')
    for q in tq:
        f_out.write(q + '\n')
    return list(tq)


def load_tq2(file):
    tq = []
    with open(file, 'r') as f:
        line = f.readline()
        while (line):
            tq.append(line.strip())
            line = f.readline()
    return tq


def load_sub(table):
    m = mymysql()
    dic = {}
    if table == "knowledge_query":
        search_command = 'select query, popularity from %s' % table
        query_list = list(m.search(search_command))
        for item in query_list:
            popular = item[1]
            if not popular: popular = 0
            if len(item[0]) < 2: continue  # 单子符过滤
            dic[item[0]] = ['', popular]
    else:
        search_command = 'select query, tag, popularity from %s' % table
        query_list = list(m.search(search_command))
        for item in query_list:
            popular = item[2]
            if not popular: popular = 0
            if len(item[0]) < 2: continue  # 单子符过滤
            dic[item[0]] = [item[1], popular]
    return dic


def load_topic_model():
    global topic_dic, model_dic
    print('load news dict ……')
    news = load_sub('news_query_tag')
    print('load shopping dict ……')
    shopping = load_sub('shopping_query_tag')
    print('load life dict ……')
    life = load_sub('life_query_tag')
    print('load entertainment dict ……')
    entertainment = load_sub('entertainment_query_tag')
    print('load knowledge dict ……')
    knowledge = load_sub('knowledge_query')
    topic_dic = {"news": news, "shopping": shopping, "life": life, "entertainment": entertainment,
                 "knowledge": knowledge}
    print('load topic dict over')

    vocab_file = "../data/vocab.txt"
    pb_model_first = "../saved_model/1"
    pb_model_news = "../saved_model/1"
    pb_model_shopping = "../saved_model/1"
    pb_model_life = "../saved_model/1"

    model_first = pb_predict(label_first, pb_model_first, vocab_file)
    model_news = pb_predict(label_news, pb_model_news, vocab_file)
    model_shopping = pb_predict(label_shopping, pb_model_shopping, vocab_file)
    model_life = pb_predict(label_life, pb_model_life, vocab_file)
    model_dic = {"first": model_first, "news": model_news, "shopping": model_shopping, "life": model_life}

    print('load topic model over')


def load_topic_cls():
    first_file = '../data/first_topic.dat'
    second_file = '../data/second_topic.dat'
    first_topic = []
    second_topic = []
    with open(first_file, 'r') as f:
        line = f.readline()
        while line:
            line = line.strip().split('\t')
            line = [float(e) for e in line]
            first_topic.append(line)
            line = f.readline()
    with open(second_file, 'r') as f:
        line = f.readline()
        while line:
            line = line.strip().split('\t')
            line = [float(e) for e in line]
            second_topic.append(line)
            line = f.readline()

    assert len(tq) == len(first_topic) == len(second_topic)

    topic = {"news": ["sports_news", "art_and_entertainment_news", "social_news", "technology_news", "business_news",
                      "political_news", "else_news"],
             "shopping": ["daily_necessities", "fashion", "electronic_product", "leisure_products", "car",
                          "else_shopping"],
             "life": ["healthcare_services", "jobs", "education", "activity", "menu", "trip", "else_life"]}
    second_cls = ["sports_news", "art_and_entertainment_news", "social_news", "technology_news", "business_news",
                  "political_news", "else_news", "daily_necessities", "fashion", "electronic_product",
                  "leisure_products",
                  "car", "else_shopping", "healthcare_services", "jobs", "education", "activity", "menu", "trip",
                  "else_life"]
    second_ind = [0, 7, 13, 20]
    topic_index = {}
    for k, v in topic.items():
        for s in v:
            topic_index[s] = k
    # 分类判断
    res = {}
    for i in range(len(tq)):
        query = tq[i]
        fir = first_topic[i]
        sec = second_topic[i]
        max1 = max(fir)
        max2 = max(sec)
        if max2 > 0.9:
            second = second_cls[sec.index(max2)]
            first = topic_index[second]
            res[query] = [first, second]
        elif max1 > 0.6:
            ind = fir.index(max2)
            first = label_first[ind]
            if first == 'else':
                res[query] = [first, '']
            tmp_topic = second_cls[second_ind[ind]:second_ind[ind + 1]]
            tmp_score = sec[second_ind[ind]:second_ind[ind + 1]]
            tmp_max = max(tmp_score)
            if tmp_max > 0.3:
                second = tmp_topic[tmp_score.index(tmp_max)]
                res[query] = [first, second]
            if max2 < 0.4:
                res[query] = [first, 'else_' + first]
        elif max2 > 0.6:
            second = second_cls[sec.index(max2)]
            first = topic_index[second]
            res[query] = [first, second]
        else:
            res[query] = ['else', '']

    return res


def model_class(query):
    probs1 = model_dic["first"].predict(query)
    max_prob1 = max(probs1)
    if max_prob1 < 0.4:
        return "else", ""
    index1 = probs1.index(max_prob1)
    first_class = label_first[index1]
    if first_class == "else":
        return "else", ""

    probs2 = model_dic[first_class].predict(query)
    max_prob2 = max(probs2)
    if max_prob2 < 0.3:
        second_class = "else_" + first_class
    else:
        index2 = probs2.index(max_prob2)
        second_class = label_first[index2]

    return first_class, second_class


def fun(query):
    first_class = 'else'
    second_class = ''
    popular = -1

    # 词典分类结果
    bol_dic = 0
    for topic, dic in topic_dic.items():
        if (query in dic) and dic[query][1] > popular:
            first_class = topic
            second_class = dic[query][0]
            popular = dic[query][1]
            bol_dic = 1

    # 模型分类结果
    if bol_dic == 0:
        # first_class, second_class = model_class(query)
        res = topic_cls[query]
        first_class = res[0]
        second_class = res[1]

    # return first_class, second_class
    return query, first_class, second_class


def fun_list():
    f = open(tq_category_file, 'a+')
    for i in range(len(tq)):
        query = tq[i]
        print(i)
        first_class = 'else'
        second_class = ''
        popular = -1

        # 词典分类结果
        bol_dic = 0
        for topic, dic in topic_dic.items():
            if (query in dic) and dic[query][1] > popular:
                first_class = topic
                second_class = dic[query][0]
                popular = dic[query][1]
                bol_dic = 1

        # 模型分类结果
        if bol_dic == 0:
            #first_class, second_class = model_class(query)
            res = topic_cls[query]
            first_class = res[0]
            second_class = res[1]

        f.write(query + '\t' + first_class + '\t' + second_class + '\n')


def write_file(data):
    f = open(tq_category_file, 'a+')
    f.write(data[0] + '\t' + data[1] + '\t' + data[2] + '\n')


if __name__ == "__main__":
    tq_file = "../data/tag_app_bak.csv"
    tq_file = "./train_query_30w.dat"
    tq_category_file = "../data/tq_category.dat"
    # tq = load_tq(tq_file)
    tq = load_tq2(tq_file)

    label_first = ['news', 'shopping', 'life', 'else']
    label_news = ["sports_news", "art_and_entertainment_news", "social_news", "technology_news", "business_news",
                  "political_news", "else_news"]
    label_shopping = ["daily_necessities", "fashion", "electronic_product", "leisure_products", "car", "else_shopping"]
    label_life = ["healthcare_services", "jobs", "education", "activity", "menu", "trip", "else_life"]
    # topic_dic = model_dic = {}
    # load_topic_model()

    topic_cls = load_topic_cls()  # 离线计算分类结果

    fun_list()

    """
    pool = Pool(15)
    pool.map(fun, tq)

    for query in sys.stdin:
        query = query.strip()
        # query = 'dress woman'
        res = fun(query)
        print(res)
    """
