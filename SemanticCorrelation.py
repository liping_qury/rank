#coding=utf-8
"""
<ori_query, train_query> correlation calculation
语义相关性计算
"""
import re
import math
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity

file_vec = '/home/liping_qury_org/some_server/word_vec/vec2.dat'
file_idf = '/home/liping_qury_org/some_server/word_vec/word_tf_idf2.dat'
#train_query_file = "/Users/edz/Downloads/train_query.dat"
stop_file = '/home/liping_qury_org/some_server/word_vec//stop.dat'


class SemanticCorrelation:
    def __init__(self):
        self.vec_dic = self.load_vec()
        self.idf_dic = self.load_idf()
        self.stop = self.load_stop()

    def load_vec(self):
        vec_dic = {}
        with open(file_vec, 'r') as f:
            line = f.readline()
            line = f.readline()
            i = 0
            while line:
                if i % 100000 == 0: print('load vec %d lines' % i)
                i += 1
                # if i>200000: break
                line = line.strip().split(' ')
                vec = line[1:]
                vec = [float(e) for e in vec]
                vec_dic[line[0]] = np.array(vec)
                line = f.readline()
        return vec_dic

    def load_idf(self):
        idf_dic = {}
        with open(file_idf, 'r') as f:
            line = f.readline()
            i = 0
            while (line):
                if i % 100000 == 0: print('load vec %d lines' % i)
                i += 1
                line = line.strip().split('\t')
                idf_dic[line[0]] = float(line[2])
                line = f.readline()
        return idf_dic

    def load_stop(self):
        stop = set()
        f = open(stop_file, 'r')
        for line in f.readlines():
            line = line.strip()
            stop.add(line)
        return stop

    def query2vec(self, query):
        query_list = re.findall(r'\b\w+\b', query)

        # stem 处理 可增减
        # query_list = [stem.stem(e) for e in query_list]  ###

        q_l = len(query_list)
        vec = np.zeros((100), dtype=np.float)
        sum_idf = 0.0
        if len(query_list) < 1:
            return 0, vec
        for term in query_list:
            try:
                idf_term = self.idf_dic[term]
            except:
                idf_term = 1.0
            try:
                vec_term = self.vec_dic[term]
            except:
                vec_term = np.array([0.1] * 100)
            # idf_term = (1 + idf_term) / 2.0
            if (term in self.stop) or (len(term) < 2): idf_term *= 0.1
            vec += vec_term * idf_term
            sum_idf += idf_term
        vec /= sum_idf
        return q_l, vec

    def train_query2vec(self, tq, query):
        ori = re.findall(r'\b\w+\b', query)
        ori = set(ori)
        # ori = set([stem.stem(e) for e in ori])

        query_list = re.findall(r'\b\w+\b', tq)

        # stem 处理 可增减
        # query_list = [stem.stem(e) for e in query_list]  ###

        q_l = len(query_list)
        vec = np.zeros((100), dtype=np.float)
        sum_idf = 0.0
        if len(query_list) < 1:
            return 0, vec
        for term in query_list:
            try:
                idf_term = self.idf_dic[term]
            except:
                idf_term = 0.5
                if term in ori: idf_term = 1.0
            try:
                vec_term = self.vec_dic[term]
            except:
                vec_term = np.array([0.1] * 100)
            # idf_term = (1 + idf_term) / 2.0
            if (term in self.stop) or (len(term) < 2): idf_term *= 0.1
            vec += vec_term * idf_term
            sum_idf += idf_term
        vec /= sum_idf
        return q_l, vec

    def clc_relevancy(self, query, title):
        query_score = {}
        q_len, vec_q = self.query2vec(query)
        # square_q = math.sqrt(np.dot(vec_q, vec_q))
        tq_len, vec_tq = self.train_query2vec(title, query)
        if (tq_len == 0) or (q_len == 0):
            return 0
        # square_tq = math.sqrt(np.dot(vec_tq, vec_tq))
        score = cosine_similarity(np.array([vec_q]), np.array([vec_tq]))
        score = score[0][0] / (1 + math.log(max(q_len, tq_len) / q_len))
        # score = np.dot(vec_q, vec_tq) / (square_q * square_tq) / (1 + math.log(max(q_len, tq_len) / q_len))

        return round(score, 5)



