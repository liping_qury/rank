# coding=utf-8
"""
es提取 query-items 数据，用于特征提取等后续训练工作
"""
import re
import math
from elasticsearch import Elasticsearch
import nltk
from random import shuffle
import time


# 数据打散
def shuff():
    tq_in = '/Users/edz/Desktop/query/data/train_query_tag.dat'
    tq_map = {}
    with open(tq_in, 'r') as f:
        line = f.readline()
        while(line):
            line_ = line.strip().split('\t')
            line = f.readline()
            if line_[0] in tq_map:
                continue
            else:
                tq_map[line_[0]] = line_[1:3]
    tq_map = [item for item in tq_map.items()]
    shuffle(tq_map)
    print('train query数据个数 %d' % len(tq_map))

    with open(tq_file, 'w') as f:
        for item in tq_map:
            f.write(item[0] + '\t' + item[1][0] + '\t' + item[1][1] + '\n')

def load_tq2():
    tq_map = {}
    with open(tq_file, 'r') as f:
        line = f.readline()
        while(line):
            line_ = line.strip().split('\t')
            line = f.readline()
            if line_[0] in tq_map: continue
            else: tq_map[line_[0]] = line_[1:3]

    print('train query数据个数 %d' % len(tq_map))
    return tq_map


def load_tq():
    tq = set()
    i = 0
    with open(tq_file, 'r') as f:
        line = f.readline()
        while(line):
            line = line.strip()
            if len(line) > 2:
                tq.add(line)
            line = f.readline()
            i += 1
            #if i>10: break
    print("load data over")
    return tq


def request_title():
    f_out = open(file_out, 'w')
    es = Elasticsearch([{'host':'34.87.48.140','port':6666}], http_auth=('elastic', 'quryes6666'), timeout=3600)
    n = 0
    print("Scrolling...")
    for query, tags in tq_map.items():
        time.sleep(0.02)
        query_json = {"query": {"match": {"query": query}}}
        es_data = es.search(index='inverted_items_prod', body=query_json, size=1000)
        Hits = es_data.get('hits').get('hits')
        Hits2 = []
        for hit in Hits:
            title = hit["_source"]["itemTitle"]
            if title and len(title) > 1:
                Hits2.append(hit)

        Hits_ = []
        if len(Hits2) < 5: continue
        elif len(Hits2) < 20:
            ind = min(10, len(Hits2))
            Hits_ += Hits2[:ind]
        else:
            Hits_ += Hits2[:4]
            Hits_ += Hits2[12:15]
            Hits_ += Hits2[-3:]

        if len(Hits_) > 10:
            print(123)
        for items in Hits_:
            itemIconPath = items["_source"]["itemIconPath"]
            picture = "0"
            if itemIconPath: picture = "1"

            app = items["_source"]["appPackageId"]
            itemAppRank = str(items["_source"]["itemAppRank"])
            itemTitle = items["_source"]["itemTitle"]
            itemLink = items["_source"]["itemLink"]
            line = [query, tags[0], tags[1], app, itemAppRank, picture, itemTitle, itemLink]
            f_out.write("\t".join(line) + "\n")

        n += 1
        if n % 10 == 0:
            print('ext data %d lines' % n)
        if n > 2000: break



if __name__ == "__main__":
    tq_file = '/Users/edz/Desktop/query/data/train_query_tag2.dat'
    file_out = '/Users/edz/Desktop/query/data/item_label.dat'

    #### tq = load_tq()

    # shuff()
    tq_map = load_tq2()

    request_title()


