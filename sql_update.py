"""
"""
import re
import sys
import math
import json
import requests
from mysqlSearch import mymysql


def load_data():
    data = []
    with open(data_file, 'r') as f:
        line = f.readline()
        while line:
            line_ = line.strip().split('\t')
            line = f.readline()
            if len(line_) != 3: print(line_)
            line_[2] = float(line_[2])
            data.append(line_)
    return data


# 更新数据  appList.dat
def data_updata():
    m = mymysql()
    i = 0
    for item in data:
        if i % 1000 == 0: print('insert %d lines' % i)
        i += 1
        item[0] = item[0].replace("'", "''")
        item[0] = item[0].replace("\\","\\\\")
        #print(item[0])
        command = "UPDATE t_train_query_app SET point = '%f' WHERE query = '%s' and app_package_id = '%s'" % (item[2], item[0], item[1])
        m.insert_single(command)
    print('data num = %d, insert over!' % len(data))


if __name__ == '__main__':

    data_file = './appList_20w.dat'

    # 批量插入
    data = load_data()
    print("all data num = %d" % len(data))
    data_updata()
